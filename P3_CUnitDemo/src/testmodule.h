/*
 * testmodule.h
 *
 *  Person list holder module.
 *  Every person has an own structure and the module has a list to hold these person structures.
 *
 *  Module has findxxx functions too.
 *
 *  Created on: Sep 4, 2016
 *      Author: zamek
 */

#ifndef TESTMODULE_H_
#define TESTMODULE_H_

#include <stdint.h>
#include <sys/queue.h>

/**
 * Limitation of the age of a person
 */
#define MAX_AGE ((UINT8_)100)

/**
 * Person's skill level
 */
enum profession_level {
	lame, newby, middle, cool, unknown
};

/**
 * Person structure
 */
struct person_t {
	char *name;		/** name of a person, cannot be NULL! */
	enum profession_level level; /** skill level of a person */
	uint8_t age;	/** age of a person must be 1 up to MAX_AGE */
	TAILQ_ENTRY(person_t) entry;	/** next person on the list */
};

/**
 * Initialize person list.
 *
 * You need to call this function first!
 *
 * \return EXIT_FAILURE if any error, or EXIT_SUCCESS if successfully
 */
uint8_t person_init();

/**
 * free the person list.
 *
 * You need to call this, if you want clear the list.
 *
 * \return EXIT_FAILURE if any error, or EXIT_SUCCESS if successfully
 */
uint8_t person_deinit();

/**
 * Create a person with the given parameters
 *
 * Warning! this function allocates memory for the new person, caller must free it!
 *
 * \param name name of the person, cannot be NULL
 * \param age age of the person, must be 1 up to MAX_AGE
 * \param level skill level of the person
 * \return a newly created person if no errror, or NULL
 */
struct person_t *person_create(char *name, uint8_t age, enum profession_level level);

/**
 * add a person to the list
 *
 * module has a list of all handled person. Module doesn't checks if the new person is already added to the list.
 *
 * \param person a newly created person to add the list. Cannot be NULL
 * \return EXIT_SUCCESS if everything is ok, or EXIT_FAILURE if any error
 */
uint8_t person_add(struct person_t *person);

/**
 * Removes a person from list.
 *
 * function doesn't checks that the person is existing in the list.
 *
 * \param person the person to remove from list. Cannot be NULL
 * \return EXIT_SUCCESS if everything is ok, or EXIT_FAILURE if any error
 */
uint8_t person_remove(struct person_t *person);

/**
 * find the first person on the list by level
 *
 * \param level wanted level
 * \return a valid person who has the wanted level or NULL if cannot found
 */
struct person_t *person_find_by_level(enum profession_level level);


/**
 * find the first person on the list by level from the given position
 *
 * \from from position to find. cannot be NULL
 * \param level wanted level
 * \return a valid person who has the wanted level or NULL if cannot found
 */
struct person_t *person_find_by_level_from(struct person_t *from, enum profession_level level);
/**
 * find the first person on the list by age
 *
 * \param age the wanted age
 * \return a valid person who has the wanted age or NULL if cannot found
 */
struct person_t *person_find_by_age(uint8_t age);

/**
 * find the first person on the list by age from the given position
 *
 * \param from from position to find. cannot be NULL
 * \param age the wanted age
 *
 * \return a valid person who has the wanted age or NULL if cannot found
 */
struct person_t *person_find_by_age_from(struct person_t *from, uint8_t age);


/**
 * get the number of persons on the list
 *
 * \return the number of persons.
 */
uint32_t person_counts();


#endif /* TESTMODULE_H_ */
