/*
 ============================================================================
 Name        : CUnitDemo.c
 Author      : Zamek
 Version     :
 Copyright   : GPL
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>

#include "testmodule.h"

#define PERSON_NUMBER 15

#define MALLOC(ptr,size) \
	do {				\
		ptr=malloc(size);	\
		if (!ptr)			\
			abort();		\
	}while(0)

#define FREE(ptr) 	\
	do {			\
		free(ptr);	\
		ptr=NULL;	\
	} while(0)

char PERSON_NAME_PREFIX[]="very long name";

int init_test(void) {
	person_init();
	return (0);
}

int close_test(void) {
	person_deinit();
	return (0);
}

void create_person_test(void) {
	struct person_t * p = person_create("CT1",
			42, cool);
	CU_ASSERT_PTR_NOT_NULL(p);
	CU_ASSERT_EQUAL(strcmp("CT1", p->name), 0);
	CU_ASSERT_EQUAL(p->level, cool);
	CU_ASSERT_EQUAL(p->age,42);
	CU_PASS("create_person test finished");
}

void add_person_test(void) {
	for (int i=0;i<PERSON_NUMBER; i++) {
		char *name=NULL;
		asprintf(&name, " %s %d", PERSON_NAME_PREFIX, i);
		struct person_t * p = person_create(name,
				(uint8_t)i+1,
				(enum profession_level) (i % unknown));
		CU_ASSERT_PTR_NOT_NULL(p);
		CU_ASSERT_EQUAL(p->age,i+1);
		CU_ASSERT_EQUAL(p->level, i%unknown);
		CU_ASSERT_EQUAL(person_add(p), EXIT_SUCCESS);
		CU_ASSERT_EQUAL(strcmp(name, p->name), 0);
	}
}

void find_first_person_by_level_test(void) {
	for (enum profession_level i=newby; i<unknown; i++) {
		struct person_t *p = person_find_by_level(i);
		CU_ASSERT_PTR_NOT_NULL(p);
	}
}

void find_first_person_by_age_test(void) {
	for (int i=0; i<PERSON_NUMBER;i++) {
		struct person_t *p = person_find_by_age(i+1);
		CU_ASSERT_PTR_NOT_NULL(p);
	}
}

int main(void) {
	CU_pSuite pSuite=NULL;

	if (CUE_SUCCESS!=CU_initialize_registry())
		return (CU_get_error());

	pSuite=CU_add_suite("Suite 1", init_test, close_test);
	if (NULL==pSuite) {
		CU_cleanup_registry();
		return (CU_get_error());
	}

	if ((NULL==CU_add_test(pSuite, "Create person test", create_person_test)) ||
		(NULL==CU_add_test(pSuite, "Add person test", add_person_test)) ||
		(NULL==CU_add_test(pSuite, "find first person by level", find_first_person_by_level_test))  ||
		(NULL==CU_add_test(pSuite, "find first person by age", find_first_person_by_age_test))) {
		CU_cleanup_registry();
		return (CU_get_error());
	}

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return (CU_get_error());
}
