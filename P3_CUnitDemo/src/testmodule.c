/**
 * testmodule.c
 *
 *  Created on: Sep 4, 2016
 *     \author: zamek
 */

#include  <stdio.h>
#include <stdlib.h>
#include "testmodule.h"

#define MALLOC(ptr,size) \
	do {				\
		ptr=malloc(size);	\
		if (!ptr)			\
			abort();		\
	}while(0)

#define FREE(ptr) 	\
	do {			\
		free(ptr);	\
		ptr=NULL;	\
	} while(0)


static TAILQ_HEAD(, person_t) tailq_head;

/**
 * Initialize person list.
 *
 * You need to call this function first!
 *
 * \return EXIT_FAILURE if any error, or EXIT_SUCCESS if successfully
 */
uint8_t person_init() {
	TAILQ_INIT(&tailq_head);
	return (EXIT_SUCCESS);
}

/**
 * free the person list.
 *
 * You need to call this, if you want clear the list.
 *
 * \return EXIT_FAILURE if any error, or EXIT_SUCCESS if successfully
 */
uint8_t person_deinit() {
	struct person_t *p;
	while(!TAILQ_EMPTY(&tailq_head)) {
		p=tailq_head.tqh_first;
		TAILQ_REMOVE(&tailq_head, tailq_head.tqh_first, entry);
		if (p) {
			FREE(p->name);
			FREE(p);
		}
	}
	return (EXIT_FAILURE);
}

/**
 * Create a person with the given parameters
 *
 * Warning! this function allocates memory for the new person, caller must free it!
 *
 * \param name name of the person, cannot be NULL
 * \param age age of the person, must be 1 up to MAX_AGE
 * \param level skill level of the person
 * \return a newly created person if no errror, or NULL
 */
struct person_t *person_create(char *name,
		uint8_t age, enum profession_level level) {
	if (!(name && age>0 && 100>=age))
		return NULL;

	struct person_t *result;
	MALLOC(result, sizeof(struct person_t));
	result->name=name;
	result->age=age;
	result->level=level;
	return (result);
}
/**
 * add a person to the list
 *
 * module has a list of all handled person. Module doesn't checks if the new person is already added to the list.
 *
 * \param person a newly created person to add the list. Cannot be NULL
 * \return EXIT_SUCCESS if everything is ok, or EXIT_FAILURE if any error
 */
uint8_t person_add(struct person_t *person) {
	if (!person)
		return (EXIT_FAILURE);

	TAILQ_INSERT_TAIL(&tailq_head, person, entry);
	return (EXIT_SUCCESS);
}


/**
 * Removes a person from list.
 *
 * function doesn't checks that the person is existing in the list.
 *
 * \param person the person to remove from list. Cannot be NULL
 * \return EXIT_SUCCESS if everything is ok, or EXIT_FAILURE if any error
 */
uint8_t person_remove(struct person_t *person) {
	if (!person)
		return (EXIT_FAILURE);

	TAILQ_REMOVE(&tailq_head, person, entry);
	return (EXIT_SUCCESS);
}


/**
 * find the first person on the list by level
 *
 * \param level wanted level
 * \return a valid person who has the wanted level or NULL if cannot found
 */
struct person_t *person_find_by_level(enum profession_level level) {
	struct person_t *p;
	TAILQ_FOREACH(p, &tailq_head, entry) {
		if (p->level==level)
			return (p);
	}

	return NULL;
}

/**
 * find the first person on the list by level from the given position
 *
 * \from from position to find. cannot be NULL
 * \param level wanted level
 * \return a valid person who has the wanted level or NULL if cannot found
 */
struct person_t *person_find_by_level_from(struct person_t *from, enum profession_level level) {
	//E You need to implement this function
}


/**
 * find the first person on the list by age
 *
 * \param age the wanted age
 * \return a valid person who has the wanted age or NULL if cannot found
 */
struct person_t *person_find_by_age(uint8_t age) {
	struct person_t *p;
	TAILQ_FOREACH(p, &tailq_head, entry) {
		if (p->age==age)
			return (p);
	}

	return NULL;
}

/**
 * find the first person on the list by age from the given position
 *
 * \param from from position to find. cannot be NULL
 * \param age the wanted age
 *
 * \return a valid person who has the wanted age or NULL if cannot found
 */
struct person_t *person_find_by_age_from(struct person_t *from, uint8_t age) {
	//E You need to implement this function
}

/**
 * get the number of persons on the list
 *
 * \return the number of persons.
 */
uint32_t person_counts() {
	//E you need to implement this function
}

