/*
 * spi.c
 *
 *  Created on: Mar 21, 2022
 *      Author: zamek
 */

#include <stdlib.h>
#include <stdbool.h>
#include <syslog.h>

#include "spi.h"
#include "macros.h"

#define LOCAL_DEBUG

#ifndef DEBUG
#undef LOCAL_DEBUG
#endif

#define MAX_POOL_SIZE 42

static TAILQ_HEAD(head, package) spi_list=
		TAILQ_HEAD_INITIALIZER(spi_list);

static TAILQ_HEAD(p_head, package) pool =
		TAILQ_HEAD_INITIALIZER(pool);

static bool initialized=false;

/**
 * \brief init SPI module
 * \param size pool size
 * \return EXIT_SUCCESS if ok, or
 *         EXIT_FAILURE if something went wrong
 */
int spi_init(int size){
#ifdef LOCAL_DEBUG
	syslog(LOG_DEBUG, "Enter spi_init, size:%d", size);
#endif
	if (size<=0 || size>MAX_POOL_SIZE)
		return EXIT_FAILURE;

	initialized=true;

	for (int i=0;i<size; i++) {
		package_t *p;
		MALLOC(sizeof(package_t), p);
		p->channel=0;
		p->value=0;
		p->valid=false;
		spi_free_package(p);
	}
	return EXIT_SUCCESS;
}

int spi_deinit() {
#ifdef LOCAL_DEBUG
syslog(LOG_DEBUG, "Enter spi_deinit");
#endif
	if (!initialized) {
		syslog(LOG_ERR, "spi_deinit not initialized yet");
		return EXIT_FAILURE;
	}

	while(! TAILQ_EMPTY(&spi_list)) {
		package_t *p=TAILQ_FIRST(&spi_list);
		if (!p) {
			TAILQ_REMOVE(&spi_list, p, next);
			FREE(p);
		}
	}

	while(! TAILQ_EMPTY(&pool)) {
		package_t *p=TAILQ_FIRST(&pool);
		if (!p) {
			TAILQ_REMOVE(&pool, p, next);
			FREE(p);
		}
	}
	initialized = false;
	return EXIT_SUCCESS;
}

/**
 * \brief add item to list
 * \param channel
 * \param value
 * \return EXIT_SUCCESS if successfully or
 * EXIT_FAILURE when item is null or something
 * went wrong
 */
int spi_add_package(uint8_t channel, uint16_t value){
	#ifdef LOCAL_DEBUG
	syslog(LOG_DEBUG, "Enter spi_add_package channel:%d, value:%d",
			channel, value);
	#endif
	if (!initialized)
		return EXIT_FAILURE;

	if (channel>=SPI_MAX_CHANNEL) {
		syslog(LOG_WARNING,
				"spi_add_package channel is ge %d",
				SPI_MAX_CHANNEL);
		return EXIT_FAILURE;
	}

	if (channel>=SPI_MAX_CHANNEL_VALUE) {
		syslog(LOG_WARNING,
			"spi_add_package channel value is ge %d",
			SPI_MAX_CHANNEL_VALUE);
		return EXIT_FAILURE;
	}

	package_t *pkg=TAILQ_FIRST(&pool);
	if (!pkg) {
		syslog(LOG_WARNING,
				"spi_add_package, pool is empty!");
		return EXIT_FAILURE;
	}

	TAILQ_REMOVE(&pool, pkg, next);
	pkg->channel=channel;
	pkg->value=value;
	pkg->valid=true;
	TAILQ_INSERT_HEAD(&spi_list, pkg, next);
	return EXIT_SUCCESS;
}

/**
 * \brief checks the list is empty?
 * \return 0 if empty or not 0 when not empty
 */
int spi_is_package(){
#ifdef LOCAL_DEBUG
syslog(LOG_DEBUG, "Enter spi_is_package");
#endif
	return initialized && ! TAILQ_EMPTY(&spi_list);
}

/**
 * Get next package from queue if exists
 * \return NULL if the queue is empty or
 *     the pointer of the next item
 */
package_t *spi_get_package(){
#ifdef LOCAL_DEBUG
syslog(LOG_DEBUG, "Enter spi_get_package");
#endif
	if (!initialized || TAILQ_EMPTY(&spi_list)) {
		syslog(LOG_ERR, "spi_get_package not initialized or empty");
		return NULL;
	}

	package_t *pkg=TAILQ_LAST(&spi_list, head);
	if (!(pkg && pkg->valid)) {
		syslog(LOG_ERR, "spi_get_package, tailq_last is null");
		return NULL;
	}

	TAILQ_REMOVE(&spi_list, pkg, next);
	return pkg;
}

/**
 * \brief free a package
 * \param package it can be NULL
 * \return EXIT_SUCCESS if successfully
 *         or EXIT_FAILURE if something went wrong
 */
int spi_free_package(package_t *package){
#ifdef LOCAL_DEBUG
syslog(LOG_DEBUG,
 "Enter spi_free_package is %s",
     package ? " not null" :"null");
#endif
	if (!(initialized && package)) {
		syslog(LOG_ERR, "spi_free_package not initialized or package is null");
		return EXIT_FAILURE;
	}

	TAILQ_INSERT_HEAD(&pool, package, next);
	return EXIT_SUCCESS;
}

