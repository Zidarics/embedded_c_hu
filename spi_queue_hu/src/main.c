/*
 * main.c
 *
 *  Created on: Mar 28, 2022
 *      Author: zamek
 */

#include <stdlib.h>
#include <syslog.h>
#include <CUnit/Basic.h>
#include "spi.h"

#define POOL_SIZE 42


// There is no init every function call must be false
int before_without_init() {
	return 0;
}

int before_with_init() {
	return spi_init(POOL_SIZE);
}

int after_without_init() {
	int res=spi_deinit();
	return res==EXIT_FAILURE?EXIT_SUCCESS:EXIT_FAILURE;
}

int after_with_init() {
	return spi_deinit();
}

//Every test will be failed because there was no init
void test_without_init() {
	CU_ASSERT_EQUAL(spi_add_package(0, 42), EXIT_FAILURE);
	CU_ASSERT_FALSE(spi_is_package());
	CU_ASSERT_PTR_NULL(spi_get_package());
	CU_ASSERT_EQUAL(spi_free_package(NULL), EXIT_FAILURE);
	CU_PASS("Test without init passed");
}

void test_with_init() {
	CU_ASSERT_FALSE(spi_is_package());
	CU_ASSERT_PTR_NULL(spi_get_package());

	CU_ASSERT_EQUAL(spi_add_package(0, 42), EXIT_SUCCESS);
	CU_ASSERT_TRUE(spi_is_package());
	package_t *p=spi_get_package();
	CU_ASSERT_PTR_NOT_NULL(p);
	//TODO check channel & value
	//TODO ispackage?
	//TOOD getpackage()

	for (int i=0;i<POOL_SIZE;++i) {
		CU_ASSERT_EQUAL(spi_add_package(i, i+142),
						EXIT_SUCCESS);
	}

	//TODO isPackage()
	//TODO add a new element will be false, because pool is empty

	for (int i=0;i<POOL_SIZE;++i) {
		//TODO getpackage & compare values
		CU_ASSERT_EQUAL(spi_free_package(p),
						EXIT_SUCCESS);
	}

	//TODO isPackage()
	//TODO getPacakge() ;


	CU_PASS("test with init passed");
}


int main(int argc, char **argv) {
   openlog("spi_queue",
		   LOG_CONS | LOG_PID | LOG_NDELAY,  LOG_LOCAL1);

   CU_pSuite without_init=NULL;
   CU_pSuite with_init=NULL;

   if (CUE_SUCCESS!=CU_initialize_registry()) {
	   syslog(LOG_ERR,
			   "CU_initialize_registry() returns with %d",
			   CU_get_error());
	   goto exit;
   }

  without_init=CU_add_suite("without init", before_without_init, after_without_init);

  if (NULL==CU_add_test(without_init, "without init check", test_without_init)) {
	  CU_cleanup_registry();
	  syslog(LOG_ERR, "addtest failed");
	  goto exit;
  }

  with_init=CU_add_suite("with init", before_with_init, after_with_init);

  if (NULL==CU_add_test(with_init,"With init tests", test_with_init)) {
	  CU_cleanup_registry();
	  syslog(LOG_ERR, "addtest failed");
	  goto exit;
  }


  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();


exit:
	closelog();
}


