/*
 * spi.h
 *
 *  Created on: Mar 21, 2022
 *      Author: zamek
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>
#include <sys/queue.h>
#include <stdbool.h>

#define SPI_MAX_CHANNEL 8
#define SPI_MAX_CHANNEL_VALUE 4096

typedef struct package {
	bool valid;
	uint8_t channel;
	uint16_t value;
	TAILQ_ENTRY(package) next;
} package_t;

/**
 * \brief init SPI module
 * \param size pool size
 * \return EXIT_SUCCESS if ok, or
 *         EXIT_FAILURE if something went wrong
 */
int spi_init(int size);

int spi_deinit();

/**
 * \brief add item to list
 * \param channel
 * \param value
 * \return EXIT_SUCCESS if successfully or
 * EXIT_FAILURE when item is null or something
 * went wrong
 */
int spi_add_package(uint8_t channel, uint16_t value);

/**
 * \brief checks the list is empty?
 * \return 0 if empty or not 0 when not empty
 */
int spi_is_package();

/**
 * Get next package from queue if exists
 * \return NULL if the queue is empty or
 *     the pointer of the next item
 */
package_t *spi_get_package();

/**
 * \brief free a package
 * \param package it can be NULL
 * \return EXIT_SUCCESS if successfully
 *         or EXIT_FAILURE if something went wrong
 */
int spi_free_package(package_t *package);

#endif /* SPI_H_ */
