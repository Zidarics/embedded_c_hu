/*
 * macros.h
 *
 *  Created on: Mar 21, 2022
 *      Author: zamek
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <stdio.h>
#include <syslog.h>

#define MALLOC(size,ptr)    \
	do {					\
		ptr=malloc(size);	\
		if (!ptr)	{		\
			syslog(LOG_EMERG, "malloc failed"); \
			abort();        \
		}					\
	} while(0)

#define FREE(ptr)			\
	do {					\
		if (ptr) {			\
			free(ptr);		\
			ptr=NULL;		\
		}					\
	} while(0)


#endif /* MACROS_H_ */
