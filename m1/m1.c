/**
 * Main entry
 *
 * \version 1.0
 * \author Zamek
 * \brief modul entry point
 * \details
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "a.h"

/**
 * \brief Entry point of the application
 * \param argc number of arguments
 * \param argv arguments cannot be null
 * \return return value from application can be EXIT_SUCCESS or
 *    EXIT_FAILURE
 */
int main(int argc, char **argv) {
	a_init();

	printf("Hello world!!\n");
	return EXIT_SUCCESS;
}
