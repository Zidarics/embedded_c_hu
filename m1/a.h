/*
 * a.h
 *
 *  Created on: Feb 28, 2022
 *      Author: zamek
 */
/**
 * \brief Module A
 *
 *  \version 1.0
 *  \author Zamek
 */
#ifndef A_H_
#define A_H_


/**
 * \brief init for module A
 * \details WARNING this function allocates memory
 * you have to release it when you no longer need it
 *
 * \return EXIT_SUCCESS or EXITR_FAILURE if something went wrong
 *
 */
int a_init();

#endif /* A_H_ */
